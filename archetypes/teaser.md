---
hidden: true
layout: "teaser"
title: "{{- $s := split .Name "-" }} {{- print (index $s 1) " / " (index $s 0)}}"
launchDate: 
date: {{ .Date }}
uploadBy:
- "ft:pedia-Redaktion"
---
