   const header = document.querySelector('header');
   const section = document.querySelector('#inhalt');
   const solrpath = "/solr/ftpedia/select"
   const max_search_results = 999

 
    function ftpediasearch() {
        var searchstring = document.getElementById('searchstring').value   // Suchtext holen
        
        let requestURL = solrpath + '?fl=resourcename,content&hl=on&hl.fl=content&hl.simple.pre=<mark>&hl.simple.post=</mark>&hl.snippets=10&hl.maxAnalyzedChars=204800&q=content:' + searchstring + '&sort=resourcename_str+desc&rows=60';
        let request = new XMLHttpRequest();
        request.open('GET', requestURL);
        request.responseType = 'json';
        request.send();
        if (header.firstChild) {
            header.removeChild(header.firstChild);
        }
        
        while (section.firstChild) {
            section.removeChild(section.firstChild)
        }
        
        request.onload = function() {
            if (request.response != null) {
               const myResponse = request.response;
               populateHeader(myResponse.response);
               showDocs(myResponse);
            }
        }
    }

    function populateHeader(jsonObj) {
      const myP = document.createElement('p');
      var numFound = jsonObj["numFound"];
      myP.textContent = "Es wurden " + numFound + " Datensätze gefunden.";
      
      myP.style.marginLeft = "0";
      myP.style.marginTop = "1em";
      header.appendChild(myP);
    }

    function showDocs(jsonObj) {
       const docs = jsonObj.response['docs'];
       const myList = document.createElement('ul');
       const myDatensatz = document.createElement('datensatz');

       for(let i = 0; i < docs.length; i++) {
         var uri = docs[i]["resourcename"];
         // var/www/ftcommunity.de/src/content/ftpedia/2019/2019-1/ftpedia-2019-1.pdf
         const uriElements = uri.toString().split("/");
         var ausgabe = uriElements[uriElements.length - 1]; 
         var linkToAusgabe = '';
         for (let i = 3; i > 0; i--) {
            linkToAusgabe = linkToAusgabe + '/';
            linkToAusgabe = linkToAusgabe + uriElements[uriElements.length - i]; 
         }
         
         var fund = document.createElement('li');
         const link = document.createElement('a');
         link.href = '/ftpedia' + linkToAusgabe;
         link.textContent = ausgabe;
         fund.appendChild(link);
         if (jsonObj.highlighting[uri]['content'] != null) {
             var highlighted = jsonObj.highlighting[uri]['content'];
             var fundlist = document.createElement('ul');
             for (let i = 0; i < highlighted.length; i++) {
                var highlightedE = document.createElement('li');
                //sinnlose Zeilenumbruchzeichen entfernen                
                var highlightedS = highlighted[i].toString().replace(/\s+/g, ' ').trim();
                highlightedE.innerHTML = highlightedS;
                fundlist.appendChild(highlightedE);
             }
             fund.appendChild(fundlist);
         }
         myList.appendChild(fund);
       }
       
       
       myDatensatz.appendChild(myList);
       section.appendChild(myDatensatz);
      
    }



