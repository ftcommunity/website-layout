   const header = document.querySelector('header');
   const section = document.querySelector('#inhalt');
   const icons = { overview: 'fas fa-images', image: 'fas fa-camera', file: 'fas fa-download', comment: 'fas fa-comment', wiki: 'fas fa-book-reader', techdoc: 'fas fa-book-reader', issue: 'fas fa-fish' };
   const icon_default = 'fas fa-list';
   const solrpath = "/solr/website/select";
   const max_search_results = 999

   
    
    function solrsearch(rowsnumber) {
        var searchstring = document.getElementById('searchstring').value   // Suchtext holen
        // Sortierreihenfolge abfragen
        var order
        if (document.getElementById('rOpt1').checked) {
            order = ''
        }
        if (document.getElementById('rOpt2').checked) {
            order = '&sort=date+desc'
        }
        if (document.getElementById('rOpt3').checked) {
            order = '&sort=date+asc'
        } 
        let requestURL = solrpath + '?fl=uri,title,layout,date,konstrukteure,fotografen,uploadBy&q=title:' + searchstring + '^2 _text_:' + searchstring+ order + '&rows=' + rowsnumber;
        if (document.getElementById("filterDate").style.display == "block" && document.getElementById('filterDateStart').value != '' && document.getElementById('filterDateEnd').value != '') {
            let dateStart = document.getElementById('filterDateStart').value;
            let dateEnd = document.getElementById('filterDateEnd').value;
            let filterString = '&fq=date:[' + dateStart + 'T00:00:00Z TO ' + dateEnd + 'T23:59:59Z]';
            requestURL = solrpath + '?fl=uri,title,layout,date,konstrukteure,fotografen,uploadBy&q=title:' + searchstring + '^2 _text_:' + searchstring + filterString + order + '&rows=' + rowsnumber;
        }
        let request = new XMLHttpRequest();
        request.open('GET', requestURL);
        request.responseType = 'json';
        request.send();
        if (header.firstChild) {
            header.removeChild(header.firstChild);
        }
        
        while (section.firstChild) {
            section.removeChild(section.firstChild)
        }
        
        request.onload = function() {
            if (request.response != null) {
               const myResponse = request.response;
               populateHeader(myResponse.response);
               showDocs(myResponse.response, rowsnumber);
            }
        }
    }

    function populateHeader(jsonObj) {
      const myP = document.createElement('p');
      var numFound = jsonObj["numFound"];
      myP.textContent = "Es wurden " + numFound + " Datensätze gefunden.";
      if (numFound > max_search_results) {
         myP.textContent = myP.textContent.concat(" Die Ergebnisliste ist jedoch auf ", max_search_results, " Datensätze beschränkt!");
      }
      myP.style.marginLeft = "0";
      myP.style.marginTop = "1em";
      header.appendChild(myP);
    }

    function showDocs(jsonObj, rowsnumber) {
       const docs = jsonObj['docs'];
       const myList = document.createElement('p');
       const myDatensatz = document.createElement('datensatz');

       for(let i = 0; i < docs.length; i++) {
          var dscrptnList = document.createElement('dl');
          var dscrptnTerm = document.createElement('dt');
          var dscrptnLoca = document.createElement('dd');
          var dscrptnTime = document.createElement('dd');
          var dscrptnKonstr = document.createElement('dd');
          var dscrptnFoto = document.createElement('dd');
          var dscrptnUpld = document.createElement('dd');

          var uri = docs[i]["uri"][0];
          var ort = "Slug: ";
          var zeit = "Datum: ";
          var konstr = "Erbaut von: ";
          var fotograf =  "Foto von: ";
          var wer = "Upload von: ";

          var layout;
          const icon = document.createElement('i');
          if (docs[i]["layout"]) {
             layout = docs[i]["layout"];
             if (icons[layout]) {
                icon.setAttribute('class', icons[layout]);
             } else {
                icon.setAttribute('class', icon_default);
             }
          } else {
             icon.setAttribute('class', 'fas fa-list');
          }
          icon.style.marginRight = "8px";
          dscrptnTerm.appendChild(icon);

          var title = docs[i]["title"];
          const link = document.createElement('a');
          link.href = uri;
          link.textContent = title;
          dscrptnTerm.appendChild(link);
          dscrptnTerm.setAttribute('class', 'resultTitle');

          const uriElements = uri.split("/");
          var ndx = 3; // vorletztes Element im URI
          if (layout == "file") {
             ndx = 2; // letztes Element im URI
          }
          ort = ort.concat(uriElements[uriElements.length-ndx]);
          dscrptnLoca.innerHTML = ort;
          dscrptnLoca.setAttribute('class', 'resultSlug');
          if (document.getElementById("sOpt1").checked) {
             dscrptnLoca.style.display = "block";
          } else {
             dscrptnLoca.style.display = "none";
          }

          if (docs[i]["date"]) {
             const zeitstempel = new Date(docs[i]["date"]);
             const tag = zeitstempel.getDate();
             const monat = zeitstempel.getMonth()+1;
             const jahr = zeitstempel.getFullYear();
             zeit = zeit.concat(tag, ".", monat, ".", jahr);
             dscrptnTime.innerHTML = zeit;
             dscrptnTime.setAttribute('class', 'resultTime');
            if (document.getElementById("sOpt2").checked) {
               dscrptnTime.style.display = "block";
            } else {
               dscrptnTime.style.display = "none";
            }
          }

          if (docs[i]["konstrukteure"] && docs[i]["konstrukteure"] != "-?-") {
            dscrptnKonstr.innerHTML = konstr.concat(docs[i]["konstrukteure"]);
            dscrptnKonstr.setAttribute('class', 'resultConstr');
            if (document.getElementById("sOpt3").checked) {
               dscrptnKonstr.style.display = "block";
            } else {
               dscrptnKonstr.style.display = "none";
            }
          }

          if (docs[i]["fotografen"] && docs[i]["fotografen"] != "-?-") {
            dscrptnFoto.innerHTML = fotograf.concat(docs[i]["fotografen"]);
            dscrptnFoto.setAttribute('class', 'resultFoto');
            if (document.getElementById("sOpt4").checked) {
               dscrptnFoto.style.display = "block";
            } else {
               dscrptnFoto.style.display = "none";
            }
          }

          if (docs[i]["uploadBy"] && docs[i]["uploadBy"] != "<no value>") {
            dscrptnUpld.innerHTML = wer.concat(docs[i]["uploadBy"]);
            dscrptnUpld.setAttribute('class', 'resultUpld');
            if (document.getElementById("sOpt5").checked) {
               dscrptnUpld.style.display = "block";
            } else {
               dscrptnUpld.style.display = "none";
            }
          }

          dscrptnList.appendChild(dscrptnTerm);
          dscrptnList.appendChild(dscrptnLoca);
          dscrptnList.appendChild(dscrptnTime);
          dscrptnList.appendChild(dscrptnKonstr);
          dscrptnList.appendChild(dscrptnFoto);
          dscrptnList.appendChild(dscrptnUpld);
          myList.appendChild(dscrptnList);
          document.getElementById("resultAdjust").style.display = "block";
       }

       myDatensatz.appendChild(myList);

       section.appendChild(myDatensatz);
       numFound = jsonObj["numFound"]; 
       if (numFound > rowsnumber)  {
          const button = document.createElement("BUTTON");
          button.className = "submitbutton";
          button.style.width = "240px";
          var str = "";
          str = str.concat("Alle ", Math.min(numFound, max_search_results), " Ergebnisse laden");
          button.innerHTML = str;
          section.appendChild(button);
          button.onclick = function() {loadall(Math.min(numFound, max_search_results))}
       }
    }

    function loadall(numFound) {
        solrsearch(numFound);
    }

    function toggleSlug() {
      var x = document.getElementById("sOpt1");
      var entries = document.getElementsByClassName("resultSlug");
      var n = entries.length;
      for (var i = 0; i< n; i++) {
         if (x.checked) {
            entries[i].style.display = "block";
         } else {
            entries[i].style.display = "none";
         }
      }
    }

    function toggleDate() {
      var x = document.getElementById("sOpt2");
      var entries = document.getElementsByClassName("resultTime");
      var n = entries.length;
      for (var i = 0; i< n; i++) {
         if (x.checked) {
            entries[i].style.display = "block";
         } else {
            entries[i].style.display = "none";
         }
      }
    }

    function toggleKonstr() {
      var x = document.getElementById("sOpt3");
      var entries = document.getElementsByClassName("resultConstr");
      var n = entries.length;
      for (var i = 0; i< n; i++) {
         if (x.checked) {
            entries[i].style.display = "block";
         } else {
            entries[i].style.display = "none";
         }
      }
    }

    function toggleFoto() {
      var x = document.getElementById("sOpt4");
      var entries = document.getElementsByClassName("resultFoto");
      var n = entries.length;
      for (var i = 0; i< n; i++) {
         if (x.checked) {
            entries[i].style.display = "block";
         } else {
            entries[i].style.display = "none";
         }
      }
    }

    function toggleUpld() {
      var x = document.getElementById("sOpt5");
      var entries = document.getElementsByClassName("resultUpld");
      var n = entries.length;
      for (var i = 0; i< n; i++) {
         if (x.checked) {
            entries[i].style.display = "block";
         } else {
            entries[i].style.display = "none";
         }
      }
    }

    function toggleSearchHelp() {
      if (document.getElementById("searchHelpBtn").value == "0") {
         document.getElementById("searchhelp").style.display = "block";
         document.getElementById("searchHelpBtn").value = "1";
         document.getElementById("searchHelpBtn").innerHTML="<i class='fas fa-angle-double-up'></i>";
      } else {
         document.getElementById("searchhelp").style.display = "none";
         document.getElementById("searchHelpBtn").value = "0";
         document.getElementById("searchHelpBtn").innerHTML="<i class='fas fa-info-circle'></i>";
      }
    }
    
    function toggleFilterDate() {
        var filterDateTrue
        if (document.getElementById("filterDateBtn").value == "0") {
         document.getElementById("filterDate").style.display = "block";
         document.getElementById("filterDateBtn").value = "1";
      } else {
         document.getElementById("filterDate").style.display = "none";
         document.getElementById("filterDateBtn").value = "0";
      }
    }
        
